#!/usr/bin/env python
# coding: utf-8

# # Palestine Israel Conflict Analysis
# #By- Aarush Kumar
# #Dated: July 16,2021

# In[1]:


import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import plotly.express as px


# In[2]:


data=pd.read_csv('/home/aarush100616/Downloads/Projects/Palestine Israel Conflict Analysis/Palestine Body Count.csv')
data


# In[3]:


data.info()


# In[4]:


data.columns


# In[5]:


data.isnull().sum()


# In[6]:


data['Palestinians Injuries']=data['Palestinians Injuries'].replace("(incl. Jun)",np.nan)
data['Israelis Injuries']=data['Israelis Injuries'].replace("(incl. Jun)",np.nan)
data['Palestinians Injuries']=data['Palestinians Injuries'].replace("(incl. Aug)",np.nan)
data['Israelis Injuries']=data['Israelis Injuries'].replace("(incl. Aug)",np.nan)


# In[7]:


data.fillna(0,inplace=True)
data


# In[8]:


data=data.iloc[0:249]
data


# In[9]:


data=data.astype({'Year':int,'Palestinians Injuries':int,'Israelis Injuries':int,'Palestinians Killed':int,'Israelis Killed':int})
data


# In[10]:


df=data[{'Year','Palestinians Injuries','Year'}]
graph=px.line(df,x='Year',y='Palestinians Injuries',color='Year',title='Palestinians Injuries',range_x=[1999,2022])
graph.show()
df=data[{'Year','Israelis Injuries'}]
graph=px.line(df,x='Year',y='Israelis Injuries',color='Year',title='Israelis Injuries',range_x=[1999,2022])
graph.show()


# In[11]:


df=data[{'Year','Palestinians Killed'}]
graph=px.line(df,x='Year',y='Palestinians Killed',color='Year',title='Palestinians Killed',range_x=[1999,2022])
graph.show()
df1=data[{'Year','Israelis Killed'}]
graph=px.line(df1,x='Year',y='Israelis Killed',color='Year',title='Israelis Killed',range_x=[1999,2022])
graph.show()


# In[12]:


plt.figure(figsize=(15,5))
plt.bar(data['Year'],data['Palestinians Killed'],label='Palestinians Killed')
plt.bar(data['Year'],data['Israelis Killed'],label='Israelis Killed')
plt.legend()
plt.show()


# In[13]:


sel=data[{'Palestinians Killed','Year'}]
fig=px.pie(sel,values='Palestinians Killed',color='Year',names='Year',labels='Year',width=800,height=600,hole=0.2)
fig.show()


# In[14]:


lis=[data["Israelis Killed"].sum(),data["Palestinians Killed"].sum()]
lab = ['Israeli','Palestinian']
plt.pie(lis,labels=lab,autopct='%1.1f%%')
plt.title('Ratio of Total Number of Killed Person In Last 20 Years')
plt.axis('equal')
plt.show()


# In[15]:


df=data[{'Palestinians Killed','Year'}]
graph=px.bar(df,x="Year",y="Palestinians Killed",color='Palestinians Killed',
              animation_frame="Year",animation_group="Palestinians Killed",log_x=True,range_x=[1999,2022],range_y=[0,2500])
graph.show()


# In[16]:


print('Total Palestinaians Killed',(data['Palestinians Killed']).sum())


# In[17]:


print('Total Israelis Killed',(data['Israelis Killed']).sum())

